# Image Segmentation Keras (Beau Hobba Version) : Implementation of Segnet, FCN, UNet, PSPNet and other models in Keras.

Increased implementation of models/code described in 
 https://divamgupta.com/image-segmentation/2019/06/06/deep-learning-semantic-segmentation-keras.html.

Credit to the base code goes to: 

Divam Gupta : https://divamgupta.com [![Twitter](https://img.shields.io/twitter/url.svg?label=Follow%20%40divamgupta&style=social&url=https%3A%2F%2Ftwitter.com%2Fdivamgupta)](https://twitter.com/divamgupta)
[Rounaq Jhunjhunu wala](https://github.com/rjalfa)


This new code was created by: 
BEAU HOBBA of University of Sydney 


## TODO  
* [x] Implement a better way to run a model from a backbone.
* [x] Different input size !!!! MORE HYPERPARAMETER ANALYSIS 
* [x] Make it so I can use the original image datasets, instead of pre-broken down data
* [x] Try implement a sliding window based image dataset 
* [: )] Save the best model of each run using a new callback 
* [x] Clean up the dumb code
* [x] Plot multiclass stuff properly (and save it!) 
* [x] Have a way to remove dumb classes (eg: not used minimal classes, so they dont interfer with the loss functions) 
* [x] Have functions to plot the data (might keep this to being in the colab only though!)
* [x] ROC curves maybe ?? 
* [x] More diagram making abilities
* [ :) - already a feature on divam gupta] A lot more prediction functions. Make the predictions a specific colour so they match the original label colours set (in MATLAB) 
* [x] MAPPING MAPPING MAPPING MAPPING MAPPING MAPPING 


## LOSS TYPES:
* 0 Weighted Categorical Crossentropy
* 1 Categorical Crossentropy
* 2 Categorical Focal (with parameterisation) 
* 3 Categorical Focal Jaccard
* 4 Dice Loss
* 5 Jaccard Loss
* 6 Categorical Focal Dice 
* 7 Categorical Focal Loss (incase something is wrong with 2)

## Change-Log: 

### 5/11/2020  - 2
* Added VGG19 Model 


### 5/11/2020
* Added evaluate_confusion_matrix(model=None, inp_images=None, annotations=None,
             inp_images_dir=None, annotations_dir=None, checkpoints_path=None, label_names=None, colour_map=plt.cm.Greens)
* Added def confusion_matrix_picture(model=None, inp_image=None, annotation=None, checkpoints_path=None, colour_map=plt.cm.Greens):
* Added  gamma, alpha to fit train params. 
* Added Losses: 5-Jaccard, 6- categorical_focal_dice_loss, 7-categorical_focal_loss  (again, in case the other one breaks)

### 16/10/2020
* Changed the predict to return a np array
* Added  test_images = True and  test_image_location = '' as train parameters. If True the code will make a prediction of a supplied image (test_image_location) on each epoch. 
* Maybe I can see if a fricken weed is detected now 

```python
class_names = ["None", "Grass", "Dirt", "Other", "Thistle", "Deadthistle", "Pattersonscurse", "Water", "Stingingnettle", "capeweed"]
save_location = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/final/vgg_unet/vggunet200.h5"
test_image = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/dataset7/images_prepped_test/Label_2_359.png"
dataset = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/dataset7/"
loading = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/testrun3/"

model.train(data_location = dataset, load_location = loading,epochs=50, 
    steps_per_epoch=526/2,input_height=224, input_width=224, optimizer_name=opt, do_augment=True, val_batch_size = 2, val_steps_per_epoch=526,  batch_size = 8, verify_dataset=False, validate=True,
    cost_matrix = weights, loss_type=2, plot_metrics=True,
    label_names=class_names, classification_reports=True , save_plot_metrics=True, gen_use_multiprocessing=True, callback_dynamic_on=True,
    test_images=True, test_image_location=test_image
    )
				   
```


### 14/10/2020
* Added new function evalute_model. Provides an evaluation of the model where you can specify the validation amount etc 

```python
def evaluate_model(model,
                   data_location=None,
                   load_location=None,
                   label_names = None
                   ):
				   
```

Adding to your code: 

```python
from keras_segmentation.train import evaluate_model
class_names = ["None", "Grass", "Dirt", "Other", "Thistle", "Deadthistle", "Pattersonscurse", "Water", "Stingingnettle", "capeweed"]

dataset = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/dataset_major/"
loading = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/testrun1/"
evaluate_model(model, data_location=dataset, load_location=loading, label_names=class_names)

```


### 13/10/2020-2
* New callbacks to save the best model
* Changed the cmap to your stock standard blue 
* Added locations to save both the most accurate and best iou models



### 13/10/2020-1  
* Changed classification plots to show accuracy per row. 
* Changed how files save. The old way was very tedious so now instead of a tonne of save locations there are two; data_location and save_location
* data_location is where your training and validation data is. Make sure you have validation data otherwise msg me and I can fix it up for you 
* save_location is where the save metrics will go (eg: class reports and metrics). You can turn these features off from saving but idk why you would
A small example of how the file should look is this: 
```python
import keras
optimizer = tf.keras.optimizers.Adagrad()
import numpy as np

opt = keras.optimizers.Adagrad(learning_rate=0.0032)
weights = [1, 1, 338, 1685, 49, 1, 1, 1, 1, 1]

class_names = ["None", "Grass", "Dirt", "Other", "Thistle", "Deadthistle", "Pattersonscurse", "Water", "Stingingnettle", "capeweed"]
save_location = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/final/vgg_unet/vggunet200.h5"

dataset = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/dataset7/"
loading = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/testrun1/"

model.train(epochs=5, data_location = dataset, load_location = loading,
    steps_per_epoch=526/2/2, input_height=224, input_width=224, optimizer_name=opt, do_augment=True, batch_size = 2, verify_dataset=False, validate=True,
    cost_matrix = weights, loss_type=2, plot_metrics=True,
    label_names=class_names, classification_reports=True , save_plot_metrics=True, gen_use_multiprocessing=True, callback_dynamic_on=True
    )

```	


### 10/10/2020 
* Fixed up classification plots a lot!
* Added a place to save classification plots in (classification_save_location)
* some more general fixes to code to make it more improved
* Small Example of the new fixes

```python
import keras
optimizer = tf.keras.optimizers.Adagrad()
import numpy as np

opt = keras.optimizers.Adagrad(learning_rate=0.0032)

weights = [1, 1, 338, 1685, 49, 1, 1, 1, 1, 1]
#weights = [1, 1, 204.561, 17.566, 132.8758, 1.5, 89.132, 1.5, 47.238, 679.93]

plot_location = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/plots/"
classification_location = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/classification/"
class_names = ["None", "Grass", "Dirt", "Other", "Thistle", "Deadthistle", "Pattersonscurse", "Water", "Stingingnettle", "capeweed"]

model.train(
    train_images =  "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/dataset7/images_prepped_train",
    train_annotations = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/dataset7/annotations_prepped_train",
    checkpoints_path = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/checkpoints19/vggunet" , epochs=2,
    val_images = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/dataset7/images_prepped_validation",
    val_annotations = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/dataset7/annotations_prepped_validation",
    csv_file_path ="/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/logs/quickrun2.log",
    steps_per_epoch=528/2/2/2, input_height=224, input_width=224, optimizer_name=opt, do_augment=True, batch_size = 2, verify_dataset=False, validate=True,
    cost_matrix = weights, loss_type=0, plot_metrics=True, plot_metrics_save_location=plot_location,
    label_names=class_names, classification_reports=True , save_plot_metrics=True, gen_use_multiprocessing=True, callback_dynamic_on=True,
    classification_save_location=classification_location
    )
)
```	

This will make confusion matrix and classification report every epoch. It will save these in classification location.
Without callback_dynamics this will just happen at the end. This should be good enough for any thesis with the evaluations this provides!
If you need the files later a convient csv file is stored for each :) 


### 9/10/2020 
* Added metrics:

```python
plot_metrics=True, plot_metrics_save_location="", save_plot_metrics=True
)
```

Adds nice plots of the accuracy, loss, f1 and iou for the model. Can specify if you want to save them and the location of where you want to save them. 



* Added Classification plots:
```python
class_names = ["None", "Grass", "Dirt", "Thistle", "Deadthistle", "Pattersonscurse", "Water", "Stingingnettle", "capeweed"]


label_names=class_names, classification_report=True , callback_dynamic_on=True
)
```
* Can now add confusion matrices and classification reports for each class. If you input a class name list it will not use IDS. Turn on classification_reports for this. 
* If you want to create a plot for each epoch, then callback_dyanmic_on can be set to true. NOTE: This only works on systems which allow multiprocessing (eg: Google Colab).  


:) 