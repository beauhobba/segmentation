import json
from .data_utils.data_loader import image_segmentation_generator, \
    verify_segmentation_dataset
import glob
import six
from keras.callbacks import Callback
from keras.callbacks import *
import numpy as np
np.random.seed(1337)
from keras.models import Sequential
from keras.utils import np_utils
import keras.backend as K
from itertools import product
from functools import partial
import tensorflow as tf
from itertools import product
import keras
from tensorflow.python.keras.utils import losses_utils
from tensorflow.keras.losses import CategoricalCrossentropy
from sklearn.metrics import *
import segmentation_models as sm
import pandas as pd
import matplotlib.pyplot as plt         # for plotting the images
from sklearn.metrics import classification_report, confusion_matrix,  multilabel_confusion_matrix, plot_confusion_matrix
import os
import itertools
from numpy import savetxt
import seaborn as sns
import shutil 
import math

def find_latest_checkpoint(checkpoints_path, fail_safe=True):

    def get_epoch_number_from_path(path):
        return path.replace(checkpoints_path, "").strip(".")

    # Get all matching files
    all_checkpoint_files = glob.glob(checkpoints_path + ".*")
    all_checkpoint_files = [ ff.replace(".index" , "" ) for ff in all_checkpoint_files ] # to make it work for newer versions of keras
    # Filter out entries where the epoc_number part is pure number
    all_checkpoint_files = list(filter(lambda f: get_epoch_number_from_path(f)
                                       .isdigit(), all_checkpoint_files))
    if not len(all_checkpoint_files):
        # The glob list is empty, don't have a checkpoints_path
        if not fail_safe:
            raise ValueError("Checkpoint path {0} invalid"
                             .format(checkpoints_path))
        else:
            return None

    # Find the checkpoint file with the maximum epoch
    latest_epoch_checkpoint = max(all_checkpoint_files,
                                  key=lambda f:
                                  int(get_epoch_number_from_path(f)))
    return latest_epoch_checkpoint


def masked_categorical_crossentropy(gt, pr):
    from keras.losses import categorical_crossentropy
    mask = 1 - gt[:, :, 0]
    return categorical_crossentropy(gt, pr) * mask


weights  = tf.constant([
       [1., 1.2, 1., 1., 1., 1., 1., 1., 1., 1.],
       [1., 1., 1.2, 1., 1., 1., 1., 1., 1., 1.],
       [1., 1., 10.9, 1.2, 1., 1., 1., 1., 1., 1.],
       [1., 0.9, 1., 1., 1., 1., 1., 1., 1., 1.],
       [1., 1., 1., 1., 1., 1., 1., 1., 1., 1.],
       [1., 1., 1., 1., 1., 1., 1., 1., 1., 1.],
       [1., 1., 1., 1., 1., 1., 1., 1., 1., 1.],
       [1., 1., 1., 1., 1., 1., 1., 1., 1., 1.],
       [1., 1., 1., 1., 1., 1., 1., 1., 1., 1.],
       [1., 1., 1., 1., 1., 1., 1., 1., 1., 1.]])
    
def weighted_categorical_crossentropy(weights):     
    def wcce(y_true, y_pred):
        Kweights = K.constant(weights)
        if not K.is_tensor(y_pred): y_pred = K.constant(y_pred)
        y_true = K.cast(y_true, y_pred.dtype)
        return K.categorical_crossentropy(y_true, y_pred) * K.sum(y_true * Kweights, axis=-1)
    return wcce
    

early_stopping = EarlyStopping(monitor='val_loss',
                               mode='min',
                               patience=30, #number of epochs with no improvement
                               verbose=1)

def csv_logger(filepath):
    return CSVLogger(filename = filepath, 
                           separator=',', 
                           append=True)

reducelr = ReduceLROnPlateau(monitor='val_loss',
                             mode='min',
                             cooldown=1,
                             patience=10, 
                             factor=0.1,
                             min_lr=0.000001,
                             verbose=1)

def model_save_best_iou(filepath): 
    return keras.callbacks.ModelCheckpoint(
        filepath=os.path.join(filepath,"bestiou.h5"),
        monitor='val_iou_score',
        save_best_only=True)


class CheckpointsCallback(Callback):
    def __init__(self, checkpoints_path):
        self.checkpoints_path = checkpoints_path

    def on_epoch_end(self, epoch, logs=None):
        if self.checkpoints_path is not None:
            self.model.save_weights(self.checkpoints_path + "." + str(epoch))
            print("saved ", self.checkpoints_path + "." + str(epoch))



def right_vers():
    print("vers 1.0")


class PerformanceVisualizationCallback(Callback):
    def __init__(self, model, validation_data, image_dir,test_img_val, image_dir_image,img):
        super().__init__()
        self.model = model
        self.validation_data = (validation_data)
        self.image_dir = image_dir
        self.test_img_val = test_img_val
        self.image_dir_image = image_dir_image
        self.img = img

    def on_epoch_end(self, epoch, logs={}):
        testX, testy = None, None
        for X, Y in self.validation_data:
            testX = X
            testY = Y
            break
        cX = (testX * 255).astype("uint8")
        cY = np.asarray( [np.argmax(p, -1) for p in testY ] )
        y_pred = np.asarray(self.model.predict(cX))
        y_true = cY            
        y_pred = np.asarray( [np.argmax(p, -1) for p in y_pred] )
        try:
            y_true = y_true[0].reshape(112*112)
            y_pred = y_pred[0].reshape(112*112)
        except:
            pass
        try:
            fig, ax = plt.subplots(figsize=(10,7))
            cm = confusion_matrix(y_true, y_pred, normalize='true')
            ax = sns.heatmap(cm, annot=True, cmap=plt.cm.Greens, cbar = False)
            plt.ylabel('True label')
            plt.xlabel('Predicted label')
            plt.title('Confusion Matrix for Epoch {}'.format(epoch))
            fig.savefig(os.path.join(self.image_dir, 'confusion_matrix_epoch_{}.png'.format(epoch)))
            cm =  np.asarray(cm)
            savetxt(os.path.join(self.image_dir,'confusion_matrix_epoch_csv_{}.csv'.format(epoch)), cm, delimiter=',')
            plt.close()
        except:
            print("Error in saving confusion matrix")
        try:
            cr = pd.DataFrame(classification_report(y_true, y_pred, output_dict=True)).transpose()
            cr.to_csv(os.path.join(self.image_dir,'classification_report_{}.csv'.format(epoch)), index= True)
        except:
            print("Error in saving classification report")
        if(self.test_img_val is True):
            try:
                print("Creating predict img")
                out = model.predict_segmentation(
                inp=self.img,
                out_fname=os.path.join(self.image_dir_image,"out_{}.png".format(epoch))
            )
            except:
                print("Error in epoch prediction")


def evaluate_model(model,
                   data_location=None,
                   load_location=None,
                   label_names = None
                   ):

    val_images = os.path.join(data_location,"images_prepped_validation")
    val_annotations = os.path.join(data_location,"annotations_prepped_validation")
    val_batch_size = 1


    list = os.listdir(val_images) # dir is your directory path
    number_files = len(list)
    print(number_files)

    val_steps_per_epoch  = int(math.floor(number_files/val_batch_size)); 
    print(val_steps_per_epoch)
    n_classes = model.n_classes
    input_height = model.input_height
    input_width = model.input_width
    output_height = model.output_height
    output_width = model.output_width

    print("EVALUATING THE MACHINE LEARNING OF: "+str(model))
    val_gen = image_segmentation_generator(
            val_images, val_annotations,  val_batch_size,
            n_classes, input_height, input_width, output_height, output_width)

    testX, testy = None, None
    for X, Y in val_gen:
        testX = X
        testY = Y
        break
    cX = (testX * 255).astype("uint8")
    cY = np.asarray( [np.argmax(p, -1) for p in testY ] )
    y_pred = np.asarray(model.predict(cX))
    y_true = cY            
    y_pred = np.asarray( [np.argmax(p, -1) for p in y_pred] )
    y_true = y_true[0].reshape(112*112)
    y_pred = y_pred[0].reshape(112*112)

    folders = ['evaluation']
    for folder in folders:
        try:
            shutil.rmtree(load_location + folder)
            os.makedirs(load_location + folder)
            print("Created: "+load_location + folder)
        except:
            os.makedirs(load_location + folder)
            print(load_location + folder)
            print("Created: "+load_location + folder)

    evaluation_save_location = os.path.join(load_location, "evaluation/")

    print('...Generating...')
    try:
        print("FINAL CONFUSION MATRIX")
        cf = confusion_matrix(y_true, y_pred, normalize='true')
        print(cf)
        cf =  np.asarray(cf)
        savetxt(os.path.join(evaluation_save_location,'FINAL_confusion_matrix_csv.csv'), cf, delimiter=',')
    except:
        print("F for confusion matrix")
    try:
        print("FINAL MULTILABEL CONFUSION MATRIX")
        mlcm = multilabel_confusion_matrix(y_true, y_pred)
        print(mlcm)
        #mlcm =  np.asarray(mlcm)
        #savetxt(os.path.join(classification_save_location,'FINAL_mulitple_confusion_matrix.csv'), cr, delimiter=',')
    except:
        print("F for multilabel confusion matrix")
    try:
        cr = classification_report(y_true, y_pred)
        print(cr)
        cr = pd.DataFrame(classification_report(y_true, y_pred, output_dict=True)).transpose()
        cr.to_csv(os.path.join(evaluation,'FINAL_classification_report_ID.csv'), index= True)
    except:
        print("NO CLASSIFICATION REPORT W ID")
    try:
        print("CLASSIFICATION REPORT w CLASS NAMES")
        cr = classification_report(y_true, y_pred, labels = label_names)
        print(cr)
    except:
        print("NO CLASSIFICATION REPORT W CLASS NAMES")
    try:
        fig, ax = plt.subplots(figsize=(10,7))
        cm = confusion_matrix(y_true, y_pred, normalize='true')
        ax = sns.heatmap(cm, annot=True, cmap=plt.cm.Greens, cbar = False)
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        plt.title('Confusion Matrix')
        fig.savefig(os.path.join(evaluation_save_location, 'FINAL_confusion_matrix_ID.png'))
        plt.show()
    except:
        print("NO ID CONFUSION MATRIX")
    try:
        fig, ax = plt.subplots(figsize=(10,7))
        cm = confusion_matrix(y_true, y_pred, normalize='true')
        ax = sns.heatmap(cm,annot=True, cmap=plt.cm.Greens, cbar = False,xticklabels=label_names,yticklabels=label_names)
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        plt.title('Confusion Matrix')
        fig.savefig(os.path.join(evaluation_save_location, 'FINAL_confusion_matrix_CLASS.png'))
        plt.show()
    except:
        print("NO CLASS NAMES CONFUSION MATRIX")


def train(model,
          data_location=None,
          load_location=None, 
          input_height=None,
          input_width=None,
          n_classes=None,
          verify_dataset=True,
          epochs=5,
          batch_size=2,
          validate=False,
          val_batch_size=2,
          auto_resume_checkpoint=False,
          load_weights=None,
          steps_per_epoch=512,
          val_steps_per_epoch=512,
          gen_use_multiprocessing=False,
          loss_type=0,
          optimizer_name='adadelta',
          do_augment=False,
          plot_metrics=False,
          augmentation_name="aug_all",
          classification_reports = False,
          label_names =None,
          save_plot_metrics = False,
          callback_dynamic_on = False, 
          test_images = True,
          test_image_location = '',
          cost_matrix = None,
          alpha = 0.25,
          gamma= 2):

    val_images = os.path.join(data_location,"images_prepped_validation")
    val_annotations = os.path.join(data_location,"annotations_prepped_validation")
    train_images = os.path.join(data_location,"images_prepped_train")
    train_annotations = os.path.join(data_location,"annotations_prepped_train")

    folders = ['logs', 'classification', 'metrics', 'checkpoints', 'saved_models', 'test_imgs']
    for folder in folders:
      try:
        shutil.rmtree(load_location + folder)
        os.makedirs(load_location + folder)
        print("Created: "+load_location + folder)
      except:
        os.makedirs(load_location + folder)
        print(load_location + folder)
        print("Created: "+load_location + folder)

    csv_file_path = os.path.join(load_location, "logs/modeltrain.log")
    plot_metrics_save_location = os.path.join(load_location, "metrics/")
    classification_save_location = os.path.join(load_location, "classification/")
    checkpoints_path = os.path.join(load_location, "checkpoints/")
    save_path = os.path.join(load_location, "saved_models/")
    test_save_location = os.path.join(load_location, "test_imgs/")

    from .models.all_models import model_from_name
    # check if user gives model name instead of the model object
    if isinstance(model, six.string_types):
        # create the model from the name
        assert (n_classes is not None), "Please provide the n_classes"
        if (input_height is not None) and (input_width is not None):
            model = model_from_name[model](
                n_classes, input_height=input_height, input_width=input_width)
        else:
            model = model_from_name[model](n_classes)

    n_classes = model.n_classes
    input_height = model.input_height
    input_width = model.input_width
    output_height = model.output_height
    output_width = model.output_width

    if validate:
        assert val_images is not None
        assert val_annotations is not None

    if optimizer_name is not None:
        if loss_type is 0:
            weights = cost_matrix
            loss_k = weighted_categorical_crossentropy(weights)
            print("Using weighted_categorical_crossentropy loss")
        elif loss_type is 1:
            loss_k = "categorical_crossentropy"
            print("Using categorical_crossentropy loss")
        elif loss_type is 2:
            loss_k = sm.losses.CategoricalFocalLoss(alpha=alpha, gamma=gamma)
            print("Using categorical_focal loss")
        elif loss_type is 3:
            loss_k = sm.losses.categorical_focal_jaccard_loss
            print("Using categorical_focal_jaccard loss")
        elif loss_type is 4:
            loss_k = sm.losses.dice_loss
            print("Using dice loss")
        elif loss_type is 5:
            loss_k = sm.losses.jaccard_loss 
            print("Using Jaccard Loss")
        elif loss_type is 6:
            loss_k = sm.losses.categorical_focal_dice_loss
            print("Using categorical_focal_dice_loss")
        elif loss_type is 7:
            loss_k = sm.losses.categorical_focal_loss 
            print("Using categorical_focal_loss ")

        else:
            weights = cost_matrix
            loss_k = weighted_categorical_crossentropy(weights)
            print("Default: weighted_categorical_crossentropy")

        model.compile(loss=loss_k,
                      optimizer=optimizer_name,
                      metrics=['accuracy', sm.metrics.f1_score, sm.metrics.iou_score])

    if checkpoints_path is not None:
        with open(checkpoints_path+"_config.json", "w") as f:
            json.dump({
                "model_class": model.model_name,
                "n_classes": n_classes,
                "input_height": input_height,
                "input_width": input_width,
                "output_height": output_height,
                "output_width": output_width
            }, f)

    if load_weights is not None and len(load_weights) > 0:
        print("Loading weights from ", load_weights)
        model.load_weights(load_weights)

    if auto_resume_checkpoint and (checkpoints_path is not None):
        latest_checkpoint = find_latest_checkpoint(checkpoints_path)
        if latest_checkpoint is not None:
            print("Loading the weights from latest checkpoint ",
                  latest_checkpoint)
            model.load_weights(latest_checkpoint)

    if verify_dataset:
        print("Verifying training dataset")
        verified = verify_segmentation_dataset(train_images,
                                               train_annotations,
                                               n_classes)
        assert verified
        if validate:
            print("Verifying validation dataset")
            verified = verify_segmentation_dataset(val_images,
                                                   val_annotations,
                                                   n_classes)
            assert verified

    train_gen = image_segmentation_generator(
        train_images, train_annotations,  batch_size,  n_classes,
        input_height, input_width, output_height, output_width,
        do_augment=do_augment, augmentation_name=augmentation_name)

    if validate:
        val_gen = image_segmentation_generator(
            val_images, val_annotations,  val_batch_size,
            n_classes, input_height, input_width, output_height, output_width)

    if callback_dynamic_on:
        gen_use_multiprocessing = True
        callbacks = [
            CheckpointsCallback(checkpoints_path),
            early_stopping,
            csv_logger(csv_file_path),
            reducelr,
            model_save_best_iou(save_path),
            PerformanceVisualizationCallback(
                                           model=model,
                                           validation_data=val_gen,
                                           image_dir=classification_save_location,
                                           test_img_val=test_images,
                                           image_dir_image=test_save_location,
                                           img=test_image_location )
        ]
    else:
        callbacks = [
            CheckpointsCallback(checkpoints_path),
            early_stopping,
            csv_logger(csv_file_path),
            reducelr,
            model_save_best_iou(save_path)
        ]
    if not validate:
        print("Not Validated")
        history = model.fit_generator(train_gen, steps_per_epoch,
                            epochs=epochs, callbacks=callbacks)
    else:
        print("Validated: VERSION 1.2")
        history = model.fit_generator(train_gen,
                            steps_per_epoch,
                            validation_data=val_gen,
                            validation_steps=val_steps_per_epoch,
                            epochs=epochs, callbacks=callbacks,
                            use_multiprocessing=gen_use_multiprocessing)
    if plot_metrics:
        print("Creating Plot Metrics...")
        metrics = pd.DataFrame(history.history)
        graph1 = metrics[["accuracy", "val_accuracy"]].plot(title='Model Accuracy')
        graph1.set_xlabel("Epochs")
        graph1.set_ylabel("Accuracy")
        graph2 =metrics[["loss", "val_loss"]].plot(title='Model Loss')
        graph2.set_xlabel("Epochs")
        graph2.set_ylabel("Loss")
        graph3 =metrics[["f1-score", "val_f1-score"]].plot(title='Model f1 Score')
        graph3.set_xlabel("Epochs")
        graph3.set_ylabel("f1-score")
        graph4 =metrics[["iou_score", "val_iou_score"]].plot(title='Model iou Score')
        graph4.set_xlabel("Epochs")
        graph4.set_ylabel("iou-score")
        if(save_plot_metrics):
            graph1.figure.savefig(os.path.join(plot_metrics_save_location, "acc.png"))
            graph2.figure.savefig(os.path.join(plot_metrics_save_location, "loss.png"))
            graph3.figure.savefig(os.path.join(plot_metrics_save_location, "f1s.png"))
            graph4.figure.savefig(os.path.join(plot_metrics_save_location, "iou.png"))

    if classification_reports:
        print("Creating classification reports...") 
        testX, testy = None, None
        for X, Y in val_gen:
            testX = X
            testY = Y
            break
        cX = (testX * 255).astype("uint8")
        cY = np.asarray( [np.argmax(p, -1) for p in testY ] )
        y_pred = np.asarray(model.predict(cX))
        y_true = cY            
        y_pred = np.asarray( [np.argmax(p, -1) for p in y_pred] )
        y_true = y_true[0].reshape(112*112)
        y_pred = y_pred[0].reshape(112*112)

        print('...Generating...')
        try:
            print("FINAL CONFUSION MATRIX")
            cf = confusion_matrix(y_true, y_pred, normalize='true')
            print(cf)
            cf =  np.asarray(cf)
            savetxt(os.path.join(classification_save_location,'FINAL_confusion_matrix_csv.csv'), cf, delimiter=',')
        except:
            print("F for confusion matrix")
        try:
            print("FINAL MULTILABEL CONFUSION MATRIX")
            mlcm = multilabel_confusion_matrix(y_true, y_pred)
            print(mlcm)
            #mlcm =  np.asarray(mlcm)
            #savetxt(os.path.join(classification_save_location,'FINAL_mulitple_confusion_matrix.csv'), cr, delimiter=',')
        except:
            print("F for multilabel confusion matrix")
        try:
            cr = classification_report(y_true, y_pred)
            print(cr)
            cr = pd.DataFrame(classification_report(y_true, y_pred, output_dict=True)).transpose()
            cr.to_csv(os.path.join(classification_save_location,'FINAL_classification_report_ID.csv'), index= True)
        except:
            print("NO CLASSIFICATION REPORT W ID")
        try:
            print("CLASSIFICATION REPORT w CLASS NAMES")
            cr = classification_report(y_true, y_pred, labels = label_names)
            print(cr)
        except:
            print("NO CLASSIFICATION REPORT W CLASS NAMES")
        try:
            fig, ax = plt.subplots(figsize=(10,7))
            cm = confusion_matrix(y_true, y_pred, normalize='all')
            ax = sns.heatmap(cm, annot=True, cmap=plt.cm.Greens, cbar = False)
            plt.ylabel('True label')
            plt.xlabel('Predicted label')
            plt.title('Confusion Matrix')
            fig.savefig(os.path.join(classification_save_location, 'FINAL_confusion_matrix_ID.png'))
            plt.show()
        except:
            print("NO ID CONFUSION MATRIX")
        try:
            fig, ax = plt.subplots(figsize=(10,7))
            cm = confusion_matrix(y_true, y_pred, normalize='all')
            ax = sns.heatmap(cm,annot=True, cmap=plt.cm.Greens, cbar = False,xticklabels=label_names,yticklabels=label_names)
            plt.ylabel('True label')
            plt.xlabel('Predicted label')
            plt.title('Confusion Matrix')
            fig.savefig(os.path.join(classification_save_location, 'FINAL_confusion_matrix_CLASS.png'))
            plt.show()
        except:
            print("NO CLASS NAMES CONFUSION MATRIX")